package com.jaguarlabs.sharedelements.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.jaguarlabs.sharedelements.R;

/**
 * Created by robertog on 12/07/16.
 */
public class BFragment extends Fragment {

    public BFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_b,container,false);

        ImageView imageView = (ImageView) root.findViewById(R.id.img_new_detail);
        EditText textView = (EditText) root.findViewById(R.id.txt_new_detail);

        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        textView.setText(getArguments().getString("NAME"));

        Glide.with(this)
                .load(getArguments().getString("IMAGE"))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .into(imageView);

        return root;
    }
}
