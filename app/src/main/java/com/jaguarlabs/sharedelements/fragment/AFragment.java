package com.jaguarlabs.sharedelements.fragment;

import android.animation.Animator;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.jaguarlabs.sharedelements.R;
import com.jaguarlabs.sharedelements.event.Notify;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by robertog on 12/07/16.
 */
public class AFragment extends Fragment implements View.OnTouchListener {

    EditText textView;
    ImageView imageView;
    Fragment endFragment;
    View root;
    ViewGroup container;
    Button option;
    private Button option2;
    private Button option3;

    String image;

    int cx, cy;
    float radius;

    public AFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_a,container,false);
        this.container = container;

        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);

        endFragment = new BFragment();

        textView = (EditText) root.findViewById(R.id.txt_new);
        imageView = (ImageView) root.findViewById(R.id.img_new);
        option = (Button) root.findViewById(R.id.option_1);
        option2 = (Button) root.findViewById(R.id.option_2);
        option3 = (Button) root.findViewById(R.id.option_3);

        image = "http://45.media.tumblr.com/b821fa9af4ddc32b03d9a3c4d33130ae/tumblr_nmi9j5Oqs81tcuj64o1_400.gif";

        Glide.with(this)
                .load(image)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.mipmap.ic_launcher)
                .into(imageView);

        setListeners();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSharedElementReturnTransition(TransitionInflater.from(
                    getActivity()).inflateTransition(R.transition.change_image_transform));
            setExitTransition(TransitionInflater.from(
                    getActivity()).inflateTransition(R.transition.change_image_transform));

            endFragment.setSharedElementEnterTransition(TransitionInflater.from(
                    getActivity()).inflateTransition(R.transition.change_image_transform));
            endFragment.setEnterTransition(TransitionInflater.from(
                    getActivity()).inflateTransition(R.transition.change_image_transform));
        }

        root.findViewById(R.id.c_1).setOnTouchListener(this);
        animateButtonsIn(false);
        return root;
    }

    private void setListeners() {
        option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radius = (float) Math.hypot(root.getWidth(), root.getHeight());

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Animator anim = ViewAnimationUtils.createCircularReveal(root, cx,
                            cy, radius, 0);

                    root.findViewById(R.id.form).animate().alpha(1).start();
                    root.findViewById(R.id.options).animate().alpha(0).start();
//                    root.setBackgroundColor(Color.WHITE);
                    anim.start();
                    anim.addListener(animatorListener);
                }
                ((FloatingActionButton) getActivity().findViewById(R.id.fab_add)).show();

            }
        });

        option2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radius = (float) Math.hypot(root.getWidth(), root.getHeight());
                cx = option2.getMeasuredWidth() / 2;
                cy = (int) option2.getY();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Animator anim = ViewAnimationUtils.createCircularReveal(root, cx,
                            cy, radius, 0);

                    root.findViewById(R.id.form).animate().alpha(1).start();
                    root.findViewById(R.id.options).animate().alpha(0).start();
//                    root.setBackgroundColor(Color.WHITE);
                    anim.start();
                    anim.addListener(animatorListener);
                }
                ((FloatingActionButton) getActivity().findViewById(R.id.fab_add)).show();

            }
        });

        option3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radius = (float) Math.hypot(root.getWidth(), root.getHeight());

                cx = root.getMeasuredWidth() / 2;
                cy = root.getMeasuredHeight() / 2;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Animator anim = ViewAnimationUtils.createCircularReveal(root, cx,
                            cy, radius, 0);

                    root.findViewById(R.id.form).animate().alpha(1).start();
                    root.findViewById(R.id.options).animate().alpha(0).start();
//                    root.setBackgroundColor(Color.WHITE);
                    anim.start();
                    anim.addListener(animatorListener);
                }
                ((FloatingActionButton) getActivity().findViewById(R.id.fab_add)).show();

            }
        });
    }

    private void animateButtonsIn(boolean reverse) {
        List<View> views = new ArrayList<>();

        views.add(root.findViewById(R.id.c_1));
        views.add(root.findViewById(R.id.c_2));
        views.add(root.findViewById(R.id.c_3));

        int direction = reverse ? 0 : 1;

        for (int i = 0; i < views.size(); i++)
            views.get(i).animate()
                .setStartDelay((i + 1) * 100)
                .alpha(direction)
                .scaleX(direction)
                .scaleY(direction)
                .start();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(Notify.EventFragments event) {

//        animateButtonsIn(true);

        Bundle bundle = new Bundle();
        bundle.putString("NAME", textView.getText().toString());
        bundle.putString("IMAGE", image);
        endFragment.setArguments(bundle);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, endFragment)
                .addToBackStack(null)
                .addSharedElement(imageView, "new_profile_image")
                .addSharedElement(textView, "new_profile_name")
                .commit();

        ((FloatingActionButton) getActivity().findViewById(R.id.fab_add)).hide();

    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int vId = v.getId();
        Animator anim = null;

        switch (vId) {
            case R.id.c_1:
                radius = (float) Math.hypot(root.getWidth(), root.getHeight());

                cx = (int) event.getRawX();
                cy = (int) event.getRawY();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    anim = ViewAnimationUtils.createCircularReveal(root, cx,
                            cy, 0, radius);
                }

                root.findViewById(R.id.form).animate().alpha(0).start();
                root.findViewById(R.id.options).animate().alpha(1).start();
                root.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                anim.start();
                ((FloatingActionButton) getActivity().findViewById(R.id.fab_add)).hide();

                break;
            case R.id.option_1:

                break;
            case R.id.c_3:
                break;
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((FloatingActionButton) getActivity().findViewById(R.id.fab_add)).show();
    }

    Animator.AnimatorListener animatorListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animation) {

        }

        @Override
        public void onAnimationEnd(Animator animation) {
            root.setBackgroundColor(Color.WHITE);

        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    };
}
