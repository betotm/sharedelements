package com.jaguarlabs.sharedelements.viewholder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jaguarlabs.sharedelements.R;

/**
 * Created by robertog on 6/07/16.
 */
public class ProfileViewHolder extends RecyclerView.ViewHolder {

    public ImageView imgProfile;
    public TextView txtProfile;
    CardView cardView;

    public ProfileViewHolder(View itemView) {
        super(itemView);

        imgProfile = (ImageView) itemView.findViewById(R.id.imgProfile);
        txtProfile = (TextView) itemView.findViewById(R.id.txtProfile);
        cardView = (CardView) itemView.findViewById(R.id.row_profile);
    }

}
