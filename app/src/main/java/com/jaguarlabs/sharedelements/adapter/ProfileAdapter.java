package com.jaguarlabs.sharedelements.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.jaguarlabs.sharedelements.R;
import com.jaguarlabs.sharedelements.model.Profile;
import com.jaguarlabs.sharedelements.viewholder.ProfileViewHolder;

import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by robertog on 6/07/16.
 */
public class ProfileAdapter extends BaseAdapter {

    List<Profile> profiles;
    private Context context;
    private LayoutInflater inflater;

    public ProfileAdapter(Context context, List<Profile> profiles) {
        this.profiles = profiles;
        this.context = context;
    }


    @Override
    public int getCount() {
        return profiles.size();
    }

    @Override
    public Object getItem(int position) {
        return profiles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.row_profile, parent, false);

        TextView txtName = (TextView) view.findViewById(R.id.txtProfile);
        ImageView imgPic = (ImageView) view.findViewById(R.id.imgProfile);

        txtName.setText(profiles.get(position).getName());
        Glide.with(context).load(profiles.get(position).getImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgPic);

        return view;
    }
}
