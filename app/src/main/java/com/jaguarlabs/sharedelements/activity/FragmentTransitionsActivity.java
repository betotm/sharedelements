package com.jaguarlabs.sharedelements.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.transition.Slide;
import android.view.Gravity;
import android.view.View;
import android.view.Window;

import com.jaguarlabs.sharedelements.R;
import com.jaguarlabs.sharedelements.event.Notify;
import com.jaguarlabs.sharedelements.fragment.AFragment;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by robertog on 12/07/16.
 */
public class FragmentTransitionsActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setEnterTransition(new Slide(Gravity.RIGHT));
            getWindow().setExitTransition(new Slide(Gravity.LEFT));
        }

        setContentView(R.layout.activity_fragment_transitions);

        FragmentManager fm = getSupportFragmentManager();

        Fragment fragment = new AFragment();

        fm.beginTransaction().add(R.id.fragment_container, fragment).commit();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_add);
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EventBus.getDefault().post(new Notify.EventFragments());
            }
        });
    }
}
