package com.jaguarlabs.sharedelements.activity;

import android.content.Intent;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.transition.Slide;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.jaguarlabs.sharedelements.R;
import com.jaguarlabs.sharedelements.adapter.ProfileAdapter;
import com.jaguarlabs.sharedelements.model.Profile;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private List<Profile> profiles = new ArrayList<>();
    private ProfileAdapter adapter;

    private ImageView sharedElement;
    private View sharedToolbar;
    private ListView profileList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setEnterTransition(new Slide(Gravity.RIGHT));
            getWindow().setExitTransition(new Slide(Gravity.LEFT));
            getWindow().setAllowEnterTransitionOverlap(false);
            getWindow().setAllowReturnTransitionOverlap(false);
        }

        setContentView(R.layout.activity_main);
        sharedToolbar = findViewById(R.id.main_toolbar);
        profileList = (ListView) findViewById(R.id.recycler_profile);
        Toolbar toolbar = (Toolbar) sharedToolbar;
        setSupportActionBar(toolbar);

        profiles.add(new Profile("https://media.bcm.edu/images/people-placeholder-profile.png", "Andrés"));
        profiles.add(new Profile("https://www.gradgreenhouse.com/resources/img/profile-placeholder-140x140.jpg", "Fabián"));
        profiles.add(new Profile("http://i.stack.imgur.com/WmvM0.png", "Carlos"));
        profiles.add(new Profile("https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTMZaRHXKOaIhbvhX2QEXRVAGTg91HaI2qn3z6GhdGxL96zPIF5lA", "Rob"));
        profiles.add(new Profile("http://jobs.myshipley.com/wp-content/uploads/2014/12/profile-placeholder-male.jpg", "Sergio"));
        profiles.add(new Profile("http://jasontracy.me/img/placeholder.png", "Churros"));
        profiles.add(new Profile("https://informatics.cas2.lehigh.edu/sites/informatics.cas2.lehigh.edu/files/styles/gallery_thumbnail/public/default_images/ProfilePlaceholderSuit.png?itok=oCcmJf1Q", "Andrés"));
        profiles.add(new Profile("http://www.ala-access.com/s/wp-content/uploads/2016/01/analyst-placeholder-avatar.png", "Fabián"));

        adapter = new ProfileAdapter(this, profiles);
        profileList.setAdapter(adapter);

        profileList.setOnItemClickListener(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_add);
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ActivityOptionsCompat options = ActivityOptionsCompat
                        .makeSceneTransitionAnimation(MainActivity.this,
                                findViewById(R.id.fab_add), "fab_add");

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    startActivity(new Intent(MainActivity.this, FragmentTransitionsActivity.class), options.toBundle());
                }
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Profile profile = profiles.get(position);

        Intent intent = new Intent(this, ScrollingActivity.class);
        intent.putExtra("EXTRA_IMAGE", profile.getImage());
        intent.putExtra("EXTRA_NAME", profile.getName());

//      multiple shared elements
//      android.support.v4.util.Pair;

        Pair<ImageView, String> p1 = Pair.create(sharedElement, "profile");
        Pair<View, String> p2 = Pair.create(sharedToolbar, "toolbar");

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {

            ActivityOptionsCompat options = ActivityOptionsCompat
                    .makeSceneTransitionAnimation(this, view.findViewById(R.id.imgProfile), "profile");
//                    .makeSceneTransitionAnimation(this, p1, p2);

            startActivity(intent, options.toBundle());
        }
    }
}
